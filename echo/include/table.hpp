// -----------------------------------------------------------------------------
// cone -- contra nesci wrapper
//
// Copyright (c) 2019 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------

#ifndef ECHO_INCLUDE_TABLE_HPP_
#define ECHO_INCLUDE_TABLE_HPP_

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

class Table {
 public:
  Table(const std::vector<std::string>& headers,
        const std::vector<size_t>& minimum_widths = {});

  ~Table();

  template <typename... T>
  void PrintValue(T&&... values) {
    std::stringstream stream;
    PrintToStream(&stream, std::forward<T>(values)...);
    PrintValueForColumn(current_column_, stream.str());
    current_column_ = (current_column_ + 1) % actual_widths.size();
    if (current_column_ == 0) {
      std::cout << '\n';
    }
  }

 private:
  std::vector<size_t> actual_widths;
  size_t current_column_ = 0;

  void PrintValueForColumn(size_t column, const std::string& value);

  template <typename T>
  static void PrintToStream(std::stringstream* stream, T&& value) {
    *stream << value;
  }

  template <typename T, typename... U>
  static void PrintToStream(std::stringstream* stream, T&& value,
                            U&&... values) {
    *stream << value;
    PrintToStream(stream, std::forward<U>(values)...);
  }
};

#endif  // ECHO_INCLUDE_TABLE_HPP_
