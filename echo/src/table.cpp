// -----------------------------------------------------------------------------
// cone -- contra nesci wrapper
//
// Copyright (c) 2019 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------

#include "table.hpp"

#include <algorithm>

Table::Table(const std::vector<std::string>& headers,
             const std::vector<size_t>& minimum_widths) {
  actual_widths.reserve(headers.size());

  for (size_t i = 0; i < headers.size(); ++i) {
    if (minimum_widths.size() > i) {
      actual_widths.push_back(
          std::max<size_t>(headers[i].size(), minimum_widths[i]));
    } else {
      actual_widths.push_back(headers[i].size());
    }
  }

  for (const auto& header : headers) {
    PrintValue(header);
  }
  size_t column = 0;
  for (auto w : actual_widths) {
    if (column == 1) {
      std::cout << "-+-";
    } else if (column > 1) {
      std::cout << '-';
    }
    for (unsigned i = 0; i < w; ++i) {
      std::cout << '-';
    }
    ++column;
  }
  std::cout << "\n";
}

Table::~Table() { std::cout << std::endl; }

void Table::PrintValueForColumn(size_t column, const std::string& value) {
  if (column == 1) {
    std::cout << " | ";
  } else if (column > 1) {
    std::cout << ' ';
  }

  if (value.size() > actual_widths[column]) {
    for (size_t i = 0; i < actual_widths[column]; ++i) {
      std::cout << '#';
    }
  } else {
    const size_t spaces_before = actual_widths[column] - value.size();

    for (size_t i = 0; i < spaces_before; ++i) {
      std::cout << ' ';
    }
    std::cout << value;
  }
}
