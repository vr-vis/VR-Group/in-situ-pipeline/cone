// -----------------------------------------------------------------------------
// cone -- contra nesci wrapper
//
// Copyright (c) 2019 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------

#include <cstring>
#include <iomanip>

#include <cone/cone.hpp>
#include "table.hpp"

[[noreturn]] void InvalidSyntax();
cone::Cone CreateConeInstance(int argc, char* argv[]);

[[noreturn]] void InvalidSyntax() {
  std::cout << "cone_echo usage:\n"
#ifdef CONE_WITH_SHARED_MEMORY
            << "cone_echo shared_memory [name]\n"
#endif
#ifdef CONE_WITH_ZEROMQ
            << "cone_echo zeromq <listen/connect> <port/address>\n"
#endif
#ifdef CONE_WITH_WEBSOCKETS
            << "cone_echo websockets <listen> <port>\n"
            << "cone_echo websockets <connect> <address> <timeout in ms>\n"
#endif
            << "cone_echo config <path_to_config_file>\n"
            << "\nparameters in square brackets are optional" << std::endl;

  std::exit(EXIT_FAILURE);
}

cone::Cone CreateConeInstance(int argc, char* argv[]) {
  (void)argv;
  if (argc < 2) {
    InvalidSyntax();
  }
#ifdef CONE_WITH_SHARED_MEMORY
  else if (std::strcmp(argv[1], "shared_memory") == 0) {
    if (argc == 2) {
      return cone::ConnectViaSharedMemory();
    } else if (argc == 3) {
      return cone::ConnectViaSharedMemory(argv[2]);
    } else {
      InvalidSyntax();
    }
  }
#endif
#ifdef CONE_WITH_ZEROMQ
  else if (std::strcmp(argv[1], "zeromq") == 0) {
    if (argc == 4) {
      if (std::strcmp(argv[2], "listen") == 0) {
        return cone::ListenForZeroMQConnection(
            static_cast<uint16_t>(std::atoi(argv[3])));
      } else if (std::strcmp(argv[2], "connect") == 0) {
        return cone::ConnectViaZeroMQ(argv[3]);
      } else {
        InvalidSyntax();
      }
    } else {
      InvalidSyntax();
    }
  }
#endif
#ifdef CONE_WITH_WEBSOCKETS
  else if (std::strcmp(argv[1], "websockets") == 0) {
    if (argc == 4) {
      if (std::strcmp(argv[2], "listen") == 0) {
        return cone::CreateWebSocketServer(
            static_cast<uint16_t>(std::atoi(argv[3])));
      } else {
        InvalidSyntax();
      }
    } else if (argc == 5) {
      if (std::strcmp(argv[2], "connect") == 0) {
        return cone::ConnectToWebsocketServer(
            argv[3], static_cast<unsigned int>(std::atoi(argv[4])));
      } else {
        InvalidSyntax();
      }
    } else {
      InvalidSyntax();
    }
  }
#endif
  else if (std::strcmp(argv[1], "config") == 0) {
    if (argc == 3) {
      return cone::ConnectUsingConfigurationFile(argv[2]);
    } else {
      InvalidSyntax();
    }
  } else {
    InvalidSyntax();
  }
}

int main(int argc, char* argv[]) {
  auto cone = CreateConeInstance(argc, argv);

  cone.SetSpikeDetectorCallback([](const auto& spike_detector_data) {
    const auto times = spike_detector_data.GetTimesteps();
    const auto neuron_ids = spike_detector_data.GetNeuronIds();

    Table t{{"Timestep", "NeuronID"}};
    for (conduit::index_t i = 0; i < times.number_of_elements(); ++i) {
      t.PrintValue(std::fixed, std::setprecision(1), times[i]);
      t.PrintValue(neuron_ids[i]);
    }
  });

  cone.SetNestMultimeterCallback([](const auto& nest_multimeter_data) {
    std::vector<std::string> table_headers;
    std::vector<size_t> column_widths;
    table_headers.push_back("NeuronID");
    column_widths.push_back(std::strlen("NeuronID"));

    const auto integer_attributes =
        nest_multimeter_data.GetIntegerAttributeNames();
    for (const auto& attribute_name : integer_attributes) {
      table_headers.push_back(attribute_name);
      column_widths.push_back(6);
    }

    const auto float_attributes =
        nest_multimeter_data.GetFloatingPointAttributeNames();
    for (const auto& attribute_name : float_attributes) {
      table_headers.push_back(attribute_name);
      column_widths.push_back(10);
    }

    std::cout << "Timestep=" << nest_multimeter_data.GetTimestep() << '\n';
    Table t{table_headers, column_widths};

    const auto neuron_ids = nest_multimeter_data.GetNeuronIds();
    for (conduit::index_t i = 0; i < neuron_ids.number_of_elements(); ++i) {
      t.PrintValue(neuron_ids[i]);
      for (const auto& attribute_name : integer_attributes) {
        t.PrintValue(
            nest_multimeter_data.GetIntegerAttributeValues(attribute_name)[i]);
      }
      for (const auto& attribute_name : float_attributes) {
        t.PrintValue(std::fixed, std::setprecision(5),
                     nest_multimeter_data.GetFloatingPointAttributeValues(
                         attribute_name)[i]);
      }
    }
  });

  while (true) {
    cone.Poll();
  }
}
