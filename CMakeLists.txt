#-------------------------------------------------------------------------------
# cone -- contra nesci wrapper
#
# Copyright (c) 2019 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualization Group.
#-------------------------------------------------------------------------------
#                                  License
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

cmake_minimum_required(VERSION 3.6.0)

project(cone)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake-utilities)

set(CMAKE_CXX_STANDARD 14)

if(UNIX AND NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

option(ENABLE_TESTS "Enable testing to ensure contra is working correctly" ON)
option(ENABLE_DEVELOPER_TESTS "Enable cppcheck and cpplint tests, only interesting for developers" OFF)

option(WITH_SHARED_MEMORY "Build with boost shared memory transport layer" OFF)
option(WITH_ZEROMQ "Build with ZeroMQ transport layer" OFF)
option(WITH_WEBSOCKETS "Build with boost Websocket transport layer" OFF)

if (NOT ENABLE_DEVELOPER_TESTS)
  set(GLOBAL_TARGET_OPTIONS NO_TESTS)
endif ()

if (ENABLE_TESTS)
  enable_testing()
  include(CTest)
endif (ENABLE_TESTS)

include(AddTarget)
add_subdirectory(cone)
add_subdirectory(echo)

configure_file(
  "cmake/cone-config.cmake"
  "${CMAKE_CURRENT_BINARY_DIR}/cone-config.cmake"
  @ONLY
)
include(CMakePackageConfigHelpers)
write_basic_package_version_file("cone-config-version.cmake"
  VERSION 19.07
  COMPATIBILITY ExactVersion
)
install(
  FILES
    "${CMAKE_BINARY_DIR}/cone-config.cmake"
    "${CMAKE_BINARY_DIR}/cone-config-version.cmake"
  DESTINATION lib/cmake/cone
)
