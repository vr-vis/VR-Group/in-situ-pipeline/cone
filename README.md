# Cone
Cone is a small wrapper for **Co**ntra and **Ne**sci and is part of the **in situ pipeline** designed for visualization and analysis of biological neural network simulation during their runtime. This library helps you to connect your visualization or analysis tool to a simulation.

You can find more details about these projects here:
* [Contra](https://devhub.vr.rwth-aachen.de/VR-Group/contra/)
* [Nesci](https://devhub.vr.rwth-aachen.de/VR-Group/nesci)
* [In situ pipeline](https://devhub.vr.rwth-aachen.de/VR-Group/in-situ-pipeline)

# Usage
The central part of the library is the *Cone* class. During initialization you can specify how to connect to the simulation and hook up callbacks for the data you are interested in.

```c++
// 1. Include cone
#include <cone/cone.hpp>

// 2. Initialization
// 2.1 Create a cone object. This can either be done using by passing a
// contra::Relay to the constructor or by using one of the utility function
// provided by Cone. For a full list of this function see the API documentation
// below.
auto cone = cone::ConnectUsingConfigurationFile("path_to_config_file.json");

// 2.2 Set up callback functions
// Again, see the API description below for a full list of callback functions.
cone.SetSpikeDetectorCallback(
  [](const auto& data) {
    // Do something...
  });

// 3. Poll Data
while (app_is_running) {
  cone.Poll(100 /* optional timeout*/);
}
```

## Example
For a complete example, look into the `echo` folder in the root of the repository.

# API

## Connection methods
### Shared Memory
These functions are only available if `-DWITH_SHARED_MEMORY=ON` was specified.
```c++
// Connect to shared memory with the default name specified in contra:
Cone ConnectViaSharedMemory();

// Connect to shared memory with a custom name
Cone ConnectViaSharedMemory(
  const std::string& shared_memory_name
);
```

### Configuration file
This is the most flexible approach to establishing a connection. The connection
details are stored in a json file at a specified path. It handles all the
previously mentioned cases.
```c++
Cone ConnectUsingConfigurationFile(
  const std::string& filename
);
```
Have a look at the files in the folder `example_configurations`.

## Callback functions
All setter methods for the callback functions accept an `std::function` so you can pass raw function pointer, lambdas, etc. The data passed to the callbacks is usually an instance of contuit view defined in `nesci::consumer`. For more information look at the [documentation of nesci](https://devhub.vr.rwth-aachen.de/VR-Group/nesci#consumer).

### Spike Detector Data
This callback is called when spike data is received.
```c++
using SpikeDetectorCallback =
  std::function<void(const nesci::consumer::SpikeDetectorDataView&)>;
```
```c++
void Cone::SetSpikeDetectorCallback(
  const SpikeDetectorCallback& callback
);
```

### Nest Multimeter Data
This callback is called when multimeter data from nest is received.
```c++
using SetNestMultimeterCallback =
  std::function<void(const nesci::consumer::SetNestMultimeterDataView&)>;
```
```c++
void Cone::SetSetNestMultimeterCallback(
  const SetNestMultimeterCallback& callback
);
```

### Other data
This callback is called whenever Cone receives data that does not match any format described by nesci.
```c++
using SetUnknownDataCallback =
  std::function<void(const conduit::Node&)>;
```
```c++
void Cone::SetSetUnknownDataCallback(
  const SetUnknownDataCallback& callback
);
```