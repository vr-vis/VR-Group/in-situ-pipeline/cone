// -----------------------------------------------------------------------------
// cone -- contra nesci wrapper
//
// Copyright (c) 2019 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------

#ifndef CONE_INCLUDE_CONE_CONE_HPP_
#define CONE_INCLUDE_CONE_CONE_HPP_

#include <cone/suppress_warnings.hpp>
#include <contra/abstract_relay.hpp>
#include <functional>
#include <nesci/consumer/nest_multimeter_data_view.hpp>
#include <nesci/consumer/spike_detector_data_view.hpp>
#include <nlohmann/json.hpp>
#include <string>

namespace cone {

SUPPRESS_WARNINGS_BEGIN_PADDED
class Cone {
 public:
  using SpikeDetectorCallback =
      std::function<void(const nesci::consumer::SpikeDetectorDataView&)>;
  using NestMultimeterCallback =
      std::function<void(const nesci::consumer::NestMultimeterDataView&)>;
  using UnknownDataCallback = std::function<void(const conduit::Node&)>;

  Cone() = delete;
  Cone(const Cone&) = delete;
  Cone(Cone&&) = default;

  Cone& operator=(const Cone&) = delete;
  Cone& operator=(Cone&&) = default;

  explicit Cone(std::unique_ptr<contra::AbstractRelay>&& relay);

  inline void SetSpikeDetectorCallback(SpikeDetectorCallback callback) {
    spike_detector_callback_ = std::move(callback);
  }

  inline void SetNestMultimeterCallback(NestMultimeterCallback callback) {
    nest_multimeter_callback_ = std::move(callback);
  }

  inline void SetUnknownDataCallback(UnknownDataCallback callback) {
    unknown_data_callback_ = std::move(callback);
  }

  void Poll(unsigned int timeout = 0);

 private:
  std::unique_ptr<contra::AbstractRelay> relay_;
  SpikeDetectorCallback spike_detector_callback_;
  NestMultimeterCallback nest_multimeter_callback_;
  UnknownDataCallback unknown_data_callback_;
};
SUPPRESS_WARNINGS_END

std::unique_ptr<contra::AbstractRelay> CreateRelayFromConfiguration(
    const nlohmann::json& connection_configuration);
Cone ConnectUsingConfigurationFile(const std::string& filename);

#ifdef CONE_WITH_SHARED_MEMORY
Cone ConnectViaSharedMemory();
Cone ConnectViaSharedMemory(const std::string& shared_memory_name);
#endif

#ifdef CONE_WITH_ZEROMQ
Cone ListenForZeroMQConnection(uint16_t port);
Cone ConnectViaZeroMQ(const std::string& address);
#endif

#ifdef CONE_WITH_WEBSOCKETS
Cone CreateWebSocketServer(uint16_t port);
Cone ConnectToWebsocketServer(const std::string& address,
                              unsigned int timeout_ms);
#endif

}  // namespace cone

#endif  // CONE_INCLUDE_CONE_CONE_HPP_
