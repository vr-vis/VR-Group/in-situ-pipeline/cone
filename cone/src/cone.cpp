// -----------------------------------------------------------------------------
// cone -- contra nesci wrapper
//
// Copyright (c) 2019 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
// -----------------------------------------------------------------------------
//                                  License
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------

#include "cone/cone.hpp"

#include <contra/relay.hpp>
#include <fstream>
#ifdef CONE_WITH_SHARED_MEMORY
#include <contra/shared_memory/shared_memory_transport.hpp>
#endif
#ifdef CONE_WITH_ZEROMQ
#include "contra/zmq/zeromq_transport.hpp"
#endif
#ifdef CONE_WITH_WEBSOCKETS
#include "contra/websockets/bwebsockets_transport_client.hpp"
#include "contra/websockets/bwebsockets_transport_server.hpp"
#endif
#include <nesci/consumer/device_data_view.hpp>

namespace cone {

Cone::Cone(std::unique_ptr<contra::AbstractRelay>&& relay)
    : relay_(std::move(relay)) {}

void Cone::Poll(unsigned int timeout) {
  const auto nodes = relay_->Receive(timeout);
  for (const auto& node : nodes) {
    auto device = nesci::consumer::DeviceDataView(&node);
    switch (device.GetType()) {
      case nesci::DataType::SPIKE_DETECTOR:
        if (spike_detector_callback_) {
          spike_detector_callback_(
              nesci::consumer::SpikeDetectorDataView{&node});
        }
        break;

      case nesci::DataType::NEST_MULTIMETER:
        if (nest_multimeter_callback_) {
          nest_multimeter_callback_(
              nesci::consumer::NestMultimeterDataView{&node});
        }
        break;

      case nesci::DataType::OTHER:
        if (unknown_data_callback_) {
          unknown_data_callback_(node);
        }
    }
  }
}

std::unique_ptr<contra::AbstractRelay> CreateRelayFromConfiguration(
    const nlohmann::json& connection_configuration) {
  const std::string connection_type = connection_configuration["type"];
  const auto& connection_options = connection_configuration["options"];

  if (connection_type == "") {
    std::cerr << "Invalid connection type: " << connection_type << std::endl;
    return nullptr;
  }
#ifdef CONE_WITH_SHARED_MEMORY
  else if (connection_type == "shared_memory") {
    if (connection_options.count("name") > 0) {
      return std::make_unique<contra::Relay<contra::SharedMemoryTransport>>(
          connection_options["name"]);
    } else {
      return std::make_unique<contra::Relay<contra::SharedMemoryTransport>>();
    }
  }
#endif
#ifdef CONE_WITH_ZEROMQ
  else if (connection_type == "zeromq_server") {
    const uint16_t port = connection_options["port"];
    return std::make_unique<contra::Relay<contra::ZMQTransport>>(port);
  } else if (connection_type == "zeromq_client") {
    const std::string address = connection_options["address"];
    return std::make_unique<contra::Relay<contra::ZMQTransport>>(address);
  }
#endif
#ifdef CONE_WITH_WEBSOCKETS
  else if (connection_type == "websocket_server") {
    const uint16_t port = connection_options["port"];
    return std::make_unique<contra::Relay<contra::BWSTServer>>(port);
  } else if (connection_type == "websocket_client") {
    const std::string address = connection_options["address"];
    const unsigned int timeout = connection_options.value("timeout", 10u);
    return std::make_unique<contra::Relay<contra::BWSTClient>>(address,
                                                               timeout);
  }
#endif
  else {
    std::cerr << "Invalid connection type: " << connection_type << std::endl;
    return nullptr;
  }
}

Cone ConnectUsingConfigurationFile(const std::string& filename) {
  nlohmann::json config;
  {
    std::ifstream file(filename);
    if (!file.is_open() || !file.good()) {
      throw std::runtime_error("Failed to open configuration file.");
    }
    file >> config;
  }
  if (config.count("connection") > 0) {
    return Cone{CreateRelayFromConfiguration(config["connection"])};
  } else {
    throw std::runtime_error(
        "Invalid configuration file: cannot find `connection` property.");
  }
}

#ifdef CONE_WITH_SHARED_MEMORY
Cone ConnectViaSharedMemory() {
  return Cone(std::make_unique<contra::Relay<contra::SharedMemoryTransport>>());
}

Cone ConnectViaSharedMemory(const std::string& shared_memory_name) {
  return Cone(std::make_unique<contra::Relay<contra::SharedMemoryTransport>>(
      shared_memory_name));
}
#endif

#ifdef CONE_WITH_ZEROMQ
Cone ListenForZeroMQConnection(uint16_t port) {
  return Cone(std::make_unique<contra::Relay<contra::ZMQTransport>>(port));
}

Cone ConnectViaZeroMQ(const std::string& address) {
  return Cone(std::make_unique<contra::Relay<contra::ZMQTransport>>(address));
}
#endif

#ifdef CONE_WITH_WEBSOCKETS
Cone CreateWebSocketServer(uint16_t port) {
  return Cone(std::make_unique<contra::Relay<contra::BWSTServer>>(port));
}

Cone ConnectToWebsocketServer(const std::string& address,
                              unsigned int timeout_ms) {
  return Cone(
      std::make_unique<contra::Relay<contra::BWSTClient>>(address, timeout_ms));
}
#endif

}  // namespace cone
